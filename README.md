# ciberseguridad
This repository has all the contents, code and labs used in the course _Ciberseguridad_

# Environment setup
## Linux
1.  Install git
    ```bash
    sudo yum -y install git
    ```
1. Install Anaconda Python   
   Follow the instructions in [Anaconda installer website](https://docs.anaconda.com/anaconda/install/linux/)
1. Install RISE
   ```
   conda install -c conda-forge rise
   ```
1. Clone this repo
    ```bash
    git clone git@gitlab.com:udem1/ciberseguridad.git
    ```
1. Change directory
   ```
   cd cibereseguridad
   ```
1. Run jupyter notebook
   ```
   jupyter notebook
   ```

## Windows
1. Install Anaconda Python   
   Follow the instructions in [Anaconda installer website](https://docs.anaconda.com/anaconda/install/windows/)
1. Install RISE
   ```
   conda install -c conda-forge rise
   ```
1. Clone this repo
    ```bash
    git clone git@gitlab.com:udem1/ciberseguridad.git
    ```
1. From anaconda bash go to the cloned directory and run
   ```
   jupyter notebook