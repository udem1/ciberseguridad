Description: Ciberseguridad UdeM Lab Template
AWSTemplateFormatVersion: 2010-09-09
Parameters:
  KeyName:
    MinLength: 1
    Type: 'AWS::EC2::KeyPair::KeyName'
  WindowsAmiId:
    Description: >-
      The ID of the Windows AMI to use. This template has only been tested with
      Windows Server 2012 R2, but may work with other versions.
    Default: ami-026419edf88a9e928 
    Type: String
  LinuxAmiId:
    Description: >-
      The ID of the Amazon Linux AMI to use. Must support cloud-init scripts to
      spin up vulnerable docker containers.
    Default: ami-0c2b8ca1dad447f8a
    Type: String
  TesterAmiId:
    Description: The ID of the Tester Linux AMI to use.
    Default:  ami-0c2b8ca1dad447f8a
    Type: String
  SourceIP:
    Description: >-
      The source CIDR address traffic into the lab will be restricted to
      (e.g.,1.2.3.4/32).
    Default: 0.0.0.0/0
    Type: String
    MinLength: '8'
    MaxLength: '41'


Resources:

  VPC:
    Type: 'AWS::EC2::VPC'
    Properties:
      CidrBlock: 192.168.0.0/16
      InstanceTenancy: default
      EnableDnsSupport: 'true'
      EnableDnsHostnames: 'true'
      Tags:
        - Key: Name
          Value: VPC
  PublicSubnet:
    Type: 'AWS::EC2::Subnet'
    Properties:
      MapPublicIpOnLaunch: true
      CidrBlock: 192.168.1.0/24
      AvailabilityZone: !Select 
        - 0
        - !GetAZs ''
      VpcId: !Ref VPC
      Tags:
        - Key: Name
          Value: PublicSubnet
  InternetGateway:
    Properties:
      Tags:
        - Key: Name
          Value: InternetGateway
    Type: 'AWS::EC2::InternetGateway'
  DHCPOptions:
    Type: 'AWS::EC2::DHCPOptions'
    Properties:
      DomainName: ec2.compute.internal
      DomainNameServers:
        - AmazonProvidedDNS
  NetworkACL:
    Type: 'AWS::EC2::NetworkAcl'
    Properties:
      VpcId: !Ref VPC
  RouteTable1:
    Type: 'AWS::EC2::RouteTable'
    Properties:
      VpcId: !Ref VPC
  RouteTable2:
    Type: 'AWS::EC2::RouteTable'
    Properties:
      VpcId: !Ref VPC

  Tester:
    Type: 'AWS::EC2::Instance'
    Properties:
      DisableApiTermination: 'false'
      
      InstanceInitiatedShutdownBehavior: stop
      BlockDeviceMappings:
        - DeviceName: /dev/sda1
          Ebs:
            VolumeSize: '40'
      Monitoring: 'false'
      ImageId: !Ref TesterAmiId
      InstanceType: t3.medium
      KeyName: !Ref KeyName
      Tags:
        - Key: Name
          Value: Tester
      NetworkInterfaces:
        - DeleteOnTermination: 'true'
          Description: Primary network interface
          DeviceIndex: 0
          SubnetId: !Ref PublicSubnet
          GroupSet:
            - !Ref LabSecurityGroup
  VulnServ:
    Type: 'AWS::EC2::Instance'
    Properties:
      DisableApiTermination: 'false'
      
      InstanceInitiatedShutdownBehavior: stop
      Monitoring: 'false'
      ImageId: !Ref LinuxAmiId
      InstanceType: m5.large
      KeyName: !Ref KeyName
      Tags:
        - Key: Name
          Value: VulnServ
      NetworkInterfaces:
        - DeleteOnTermination: 'true'
          Description: Primary network interface
          DeviceIndex: 0
          SubnetId: !Ref PublicSubnet
          GroupSet:
            - !Ref LabSecurityGroup
      UserData: !Base64 
        'Fn::Join':
          - |+

          - - |
              #!/bin/bash
            - |
              sudo yum update -y
            - |
              sudo yum install -y docker
            - |
              sudo service docker start 
            - |
              sudo chkconfig docker on
            - |
              sudo docker pull bkimminich/juice-shop
            - >
              sudo docker run --name juice-shop --rm -d -p 3000:3000
              bkimminich/juice-shop
            - >
              sudo curl -L
              "https://github.com/docker/compose/releases/download/1.24.1/docker-compose-$(uname
              -s)-$(uname -m)" -o /usr/local/bin/docker-compose
            - |
              sudo chmod +x /usr/local/bin/docker-compose
            - |
              sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose
            - |
              mkdir /home/ec2-user/webgoat && cd /home/ec2-user/webgoat
            - >
              sudo curl
              https://raw.githubusercontent.com/WebGoat/WebGoat/develop/docker-compose.yml
              > docker-compose.yml
            - |
              sudo docker-compose up -d
  WindowsServer:
    Type: 'AWS::EC2::Instance'
    Properties:
      ImageId: !Ref WindowsAmiId
      InstanceType: m5.large
      
      DisableApiTermination: 'false'
      InstanceInitiatedShutdownBehavior: stop
      Monitoring: 'false'
      KeyName: !Ref KeyName
      Tags:
        - Key: Name
          Value: Windows Member Server
      NetworkInterfaces:
        - DeleteOnTermination: 'true'
          Description: Primary network interface
          DeviceIndex: 0
          SubnetId: !Ref PublicSubnet
          GroupSet:
            - !Ref LabSecurityGroup

  LabSecurityGroup:
    Type: 'AWS::EC2::SecurityGroup'
    Properties:
      GroupDescription: 'Permits all outbound, permits inbound via SSH and RDP from my IP'
      VpcId: !Ref VPC
  acl1:
    Type: 'AWS::EC2::NetworkAclEntry'
    Properties:
      CidrBlock: 0.0.0.0/0
      Egress: 'true'
      Protocol: '-1'
      RuleAction: allow
      RuleNumber: '100'
      NetworkAclId: !Ref NetworkACL
  acl2:
    Type: 'AWS::EC2::NetworkAclEntry'
    Properties:
      CidrBlock: 0.0.0.0/0
      Protocol: '-1'
      RuleAction: allow
      RuleNumber: '100'
      NetworkAclId: !Ref NetworkACL
  subnetacl1:
    Type: 'AWS::EC2::SubnetNetworkAclAssociation'
    Properties:
      NetworkAclId: !Ref NetworkACL
      SubnetId: !Ref PublicSubnet
  InternetGatewayAttachment:
    Properties:
      InternetGatewayId: !Ref InternetGateway
      VpcId: !Ref VPC
    Type: 'AWS::EC2::VPCGatewayAttachment'
  subnetroute1:
    Type: 'AWS::EC2::SubnetRouteTableAssociation'
    Properties:
      RouteTableId: !Ref RouteTable1
      SubnetId: !Ref PublicSubnet
  route1:
    Type: 'AWS::EC2::Route'
    Properties:
      DestinationCidrBlock: 0.0.0.0/0
      RouteTableId: !Ref RouteTable1
      GatewayId: !Ref InternetGateway
    DependsOn: InternetGateway
  DHCPAssoc:
    Type: 'AWS::EC2::VPCDHCPOptionsAssociation'
    Properties:
      VpcId: !Ref VPC
      DhcpOptionsId: !Ref DHCPOptions
  ingress1:
    Type: 'AWS::EC2::SecurityGroupIngress'
    Properties:
      GroupId: !Ref LabSecurityGroup
      IpProtocol: '-1'
      CidrIp: 192.168.0.0/16
  ingress2:
    Type: 'AWS::EC2::SecurityGroupIngress'
    Properties:
      GroupId: !Ref LabSecurityGroup
      IpProtocol: tcp
      FromPort: '22'
      ToPort: '22'
      CidrIp: !Ref SourceIP
  ingress3:
    Type: 'AWS::EC2::SecurityGroupIngress'
    Properties:
      GroupId: !Ref LabSecurityGroup
      IpProtocol: tcp
      FromPort: '3389'
      ToPort: '3389'
      CidrIp: !Ref SourceIP
  ingress4:
    Type: 'AWS::EC2::SecurityGroupIngress'
    Properties:
      GroupId: !Ref LabSecurityGroup
      IpProtocol: icmp
      FromPort: '-1'
      ToPort: '-1'
      CidrIp: !Ref SourceIP
  ingress5:
    Type: 'AWS::EC2::SecurityGroupIngress'
    Properties:
      GroupId: !Ref LabSecurityGroup
      IpProtocol: tcp
      FromPort: '3000'
      ToPort: '3000'
      CidrIp: !Ref SourceIP
  ingress6:
    Type: 'AWS::EC2::SecurityGroupIngress'
    Properties:
      GroupId: !Ref LabSecurityGroup
      IpProtocol: tcp
      FromPort: '8080'
      ToPort: '8080'
      CidrIp: !Ref SourceIP
  ingress7:
    Type: 'AWS::EC2::SecurityGroupIngress'
    Properties:
      GroupId: !Ref LabSecurityGroup
      IpProtocol: tcp
      FromPort: '9001'
      ToPort: '9001'
      CidrIp: !Ref SourceIP
  ingress8:
    Type: 'AWS::EC2::SecurityGroupIngress'
    Properties:
      GroupId: !Ref LabSecurityGroup
      IpProtocol: tcp
      FromPort: '9090'
      ToPort: '9090'
      CidrIp: !Ref SourceIP
  egress1:
    Type: 'AWS::EC2::SecurityGroupEgress'
    Properties:
      GroupId: !Ref LabSecurityGroup
      IpProtocol: '-1'
      CidrIp: 0.0.0.0/0
